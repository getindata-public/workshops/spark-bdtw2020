import scala.util.Properties

name := "production-ready-spark-app"

version := "0.1-SNAPSHOT"

organization := "com.getindata"

scalaVersion := "2.11.8"

val DEFAULT_SPARK_2_VERSION = "2.4.4"

lazy val sparkVersion = Properties.envOrElse("SPARK_VERSION", DEFAULT_SPARK_2_VERSION)

libraryDependencies ++= Seq(
  "org.apache.spark" % "spark-core_2.11" % sparkVersion % "provided",
  "org.apache.spark" % "spark-sql_2.11" % sparkVersion % "provided",
  "org.apache.spark" %% "spark-hive" % sparkVersion % "provided",
  "org.apache.spark" %% "spark-hive-thriftserver" % sparkVersion % "provided",
  "org.apache.spark" %% "spark-mllib" % sparkVersion % "provided",
  "com.holdenkarau" % "spark-testing-base_2.11" % "2.4.3_0.12.0" % "test" excludeAll ExclusionRule(organization = "javax.servlet") excludeAll ExclusionRule("org.apache.hadoop"),

  "org.rogach" %% "scallop" % "3.1.2",
  
  "com.amazon.deequ" % "deequ" % "1.0.1"
)

// need below two lines to include "provided" dependecies on the classpath when running in IntelliJ
run in Compile := Defaults.runTask(fullClasspath in Compile, mainClass in(Compile, run), runner in(Compile, run)).evaluated
runMain in Compile := Defaults.runMainTask(fullClasspath in Compile, runner in(Compile, run)).evaluated

fork := true
fork in Test := true

cleanKeepFiles ++= Seq("resolution-cache", "streams").map(target.value / _)

clean := Defaults.doClean(cleanFiles.value, cleanKeepFiles.value ++
  (ivyConfiguration.value match {
    case i: InlineIvyConfiguration => i.resolutionCacheDir
    case _ => None
  }).toList)

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs@_*) => MergeStrategy.discard
  case x => MergeStrategy.first
}
